var Device = require('zetta-device');
var util = require('util');

var configDefaults = {
  quiet: false,
  randomEvents: true,
  changeIntervalMin: 5000,
  changeIntervalMax: 20000
};

var motionsStates = {
  active: 'Active',
  inactive: 'Inactive'
};

var motionToggle = {}
motionToggle[motionsStates.active] = motionsStates.inactive;
motionToggle[motionsStates.inactive] = motionsStates.active;

function randomBound(low,high) {
  return Math.random() * (high-low) + low;
}

// Name defaults to 'motion' if not provided
var MotionSensor = module.exports = function(name, options) {
  Device.call(this);
  this.name = name || 'motion';

  var config = getConfig(options);
  if(config.quiet) {
    // quiet: `log` => `noop`
    log = function(){}
  }

  this.motion = motionsStates.inactive;
  this.mock_RandomEvents = config.randomEvents;

  // some time later, toggle the state
  var nextTick = function() {
    return randomBound(config.changeIntervalMin, config.changeIntervalMax)
  };
  this._tick = this._tick.bind(this, nextTick);
  this._tick();
}

util.inherits(MotionSensor, Device);

MotionSensor.prototype.init = function(config) {
  config
    .name(this.name)
    .state('ready')
    .type('occupancy')
    .monitor('motion')
    .map('mock_SetRandomEvents', this.setRandomEvents, [
      {name:'randomEvents', type:'radio', value:[
        {value:'on'}, {value:'off'}
      ]}
    ])
    .map('mock_TriggerMotion', this.trigger)
    .map('mock_ClearMotion', this.clear)
    .map('mock_TriggerMotionToggle', this.toggle)
    .when('ready', {
      allow: [
        'mock_SetRandomEvents',
        'mock_TriggerMotion',
        'mock_ClearMotion',
        'mock_TriggerMotionToggle'
      ]
    })
}

MotionSensor.prototype.setRandomEvents = function(state, cb) {
  var on = state === 'on';
  this.mock_RandomEvents = on;
  cb();
}

MotionSensor.prototype.trigger = function(cb) {
  this.motion = motionsStates.active;
  cb();
}

MotionSensor.prototype.clear = function(cb) {
  this.motion = motionsStates.inactive;
  cb();
}

MotionSensor.prototype.toggle = function(cb) {
  this.motion = motionToggle[this.motion];
  cb();
}

MotionSensor.prototype._tick = function(nextTick) {
  setTimeout(function(){
    if(this.mock_RandomEvents) {
      this.motion = motionToggle[this.motion];
    }
    this._tick();
  }.bind(this), nextTick());
};

function log(message) {
  console.log(message);
}

function getConfig(options) {
  var result = {};

  options = options || {};
  if(typeof options === 'string') {
    try { options = JSON.parse(options); }
    catch(err) { console.log(err); }
  }

  Object.keys(configDefaults).forEach(function(prop){
    var override = options.hasOwnProperty(prop);
    result[prop] = override ? options[prop] : configDefaults[prop];
  });

  return result;
}